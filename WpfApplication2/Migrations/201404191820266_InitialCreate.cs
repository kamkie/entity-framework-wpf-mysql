namespace WpfApplication2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.articles",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        title = c.String(unicode: false),
                        body = c.String(unicode: false),
                        autor = c.String(unicode: false),
                        created = c.DateTime(precision: 0),
                        modified = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.books",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        version = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.hotels",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        version = c.Long(nullable: false),
                        country = c.Int(nullable: false),
                        gid = c.Int(nullable: false),
                        name = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.people",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        version = c.Long(nullable: false),
                        first_name = c.String(unicode: false),
                        last_name = c.String(unicode: false),
                        middle_name = c.String(unicode: false),
                        section = c.String(unicode: false),
                        studentid = c.String(unicode: false),
                        year = c.Int(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.students",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        version = c.Long(nullable: false),
                        first_name = c.String(unicode: false),
                        last_name = c.String(unicode: false),
                        middle_name = c.String(unicode: false),
                        section = c.String(unicode: false),
                        studentid = c.String(unicode: false),
                        year = c.Int(),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.students");
            DropTable("dbo.people");
            DropTable("dbo.hotels");
            DropTable("dbo.books");
            DropTable("dbo.articles");
        }
    }
}
