namespace WpfApplication2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.articles", "type", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.articles", "type");
        }
    }
}
