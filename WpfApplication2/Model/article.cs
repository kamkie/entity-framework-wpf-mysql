namespace WpfApplication2.Model
{
    using System;
    using System.Collections.Generic;
    
    public class Article
    {
        public int id { get; set; }
        public string title { get; set; }
        public string body { get; set; }
        public string autor { get; set; }
        public string type { get; set; }
        public Nullable<System.DateTime> created { get; set; }
        public Nullable<System.DateTime> modified { get; set; }

    }
}
