﻿namespace WpfApplication2.Model
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;

    using WpfApplication2.Migrations;

    public partial class CakephpEntities : DbContext
    {
        public CakephpEntities()
            : base("name=cakephpEntities")
        {

        }

        public virtual DbSet<Article> Articles { get; set; }
        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<Hotel> Hotels { get; set; }
        public virtual DbSet<Person> People { get; set; }
        public virtual DbSet<Student> Students { get; set; }
    }
}
