namespace WpfApplication2.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Hotel
    {
        public int id { get; set; }
        public long version { get; set; }
        public int country { get; set; }
        public int gid { get; set; }
        public string name { get; set; }
    }
}
