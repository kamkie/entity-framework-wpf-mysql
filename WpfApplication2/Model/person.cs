namespace WpfApplication2.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class Person
    {
        public int id { get; set; }
        public long version { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string middle_name { get; set; }
        public string section { get; set; }
        public string studentid { get; set; }
        public Nullable<int> year { get; set; }
    }
}
