﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using WpfApplication2.Model;

namespace WpfApplication2.ViewModel
{
    class ArticlesGrid
    {
        CakephpEntities context = new CakephpEntities();
        ObservableCollection<Article> articles = new ObservableCollection<Article>();

        public ArticlesGrid()
        {
            var articlesDB = context.Articles;
            foreach (var row in articlesDB)
            {
                articles.Add(row);
            }
        }

        public ObservableCollection<Article> Articles
        {
            get { return articles; }
        }

        public CakephpEntities Context
        {
            get { return context; }
        }



    }
}
